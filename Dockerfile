FROM Largitto/Largitto.github.io:latest

LABEL "com.github.actions.name"="KT NPM"
LABEL "com.github.actions.description"="Run NPM"
LABEL "com.github.actions.icon"="code"
LABEL "com.github.actions.color"="purple"

LABEL "repository"="https://github.com/Largitto/Largitto.github.io"
LABEL "homepage"="https://github.com/Largitto/Largitto.github.io"
LABEL "maintainer"="Largitto <true.gentleman8808@gmail.com>"

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
