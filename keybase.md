### Keybase proof

I hereby claim:

  * I am largitto on github.
  * I am largitto (https://keybase.io/largitto) on keybase.
  * I have a public key ASAtrI4_iCdjBq5ljhg86H31DtGwjuEoZz2wC46IgRTqsQo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "01202dac8e3f88276306ae658e183ce87df50ed1b08ee128673db00b8e888114eab10a",
      "host": "keybase.io",
      "kid": "01202dac8e3f88276306ae658e183ce87df50ed1b08ee128673db00b8e888114eab10a",
      "uid": "c706d116bc4564acf142515361ea9519",
      "username": "largitto"
    },
    "merkle_root": {
      "ctime": 1596131747,
      "hash": "f24bd22c2ba81d800cb31a52990861dbf9091e301db4dd1c950fed575d0efe2d8e22066ff81bc99c3e0299dc32b35e602a8152d29e1a02eda67ce72bcb14f1e1",
      "hash_meta": "d5914ac874f084e778dc5b7702483e03f9e5fc59e39cbc5320c9ab3858af293f",
      "seqno": 17135613
    },
    "service": {
      "entropy": "rZBSfwZ9QwJegAez7fBPEnGk",
      "name": "github",
      "username": "largitto"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.5.0"
  },
  "ctime": 1596131758,
  "expire_in": 504576000,
  "prev": "c95ecda554a7a73c8ae92ed1e6119c38a47504bbd1693f78e28a578c4b099685",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASAtrI4_iCdjBq5ljhg86H31DtGwjuEoZz2wC46IgRTqsQo](https://keybase.io/largitto), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgLayOP4gnYwauZY4YPOh99Q7RsI7hKGc9sAuOiIEU6rEKp3BheWxvYWTESpcCBMQgyV7NpVSnpzyK6S7R5hGcOKR1BLvRaT944opXjEsJloXEIKx9HNblFN4FC1m1y9SQKn9ZLwJ4sY+vdbSfvvRw07LWAgHCo3NpZ8RAvszsPpxWN4xXkak8BIeMkuveIfKE4vYvKrpZc8v6SBuiUEZJJT3s+GhiFGUqUTlPq1sKLvv5xf5cxvInFapnB6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIADyH+Mt1mG/5grMLVshw08UP+mElVT5hvYrQT8Lkz+co3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/largitto

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id largitto
```
